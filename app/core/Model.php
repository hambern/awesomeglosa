<?php

class Model
{
    protected $db;

    public $table;

    public function __construct()
    {
        $config = require('app/config.php');

        $dsn = "mysql:host={$config['database']['host']};port={$config['database']['port']};dbname={$config['database']['dbname']};charset=utf8mb4";

        try {
            $this->db = new PDO($dsn, $config['database']['user'], $config['database']['password'], [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    public function query(string $sql, array $params = [])
    {
        $stmt = $this->db->prepare($sql);

        foreach ($params as $key => $value) {
            $stmt->bindValue(":$key", $value, PDO::PARAM_STR);
        }

        $stmt->execute();

        if (preg_match('/^INSERT/i', $sql)) {
            return $this->db->lastInsertId();
        } elseif (preg_match('/^(UPDATE|DELETE)/i', $sql)) {
            return $stmt->rowCount();
        } else {
            return $stmt;
        }
    }

    public function find(int $id)
    {
        return $this->query("SELECT * FROM $this->table WHERE id = :id", compact('id'))->fetch();
    }

    public function all()
    {
        return $this->query("SELECT * FROM $this->table")->fetchAll();
    }
}
