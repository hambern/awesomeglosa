<?php

class Controller
{
    public function index()
    {
        $error = 'Not a valid endpoint';
        $code = 404;

        $this->json(compact('error', 'code'));
    }

    protected function auth()
    {
        $token = $_POST['token'] ?? null;
        $error = null;

        if (!$token) {
            $code = 401;
            $error = 'No token provided';
            $this->json(compact('code', 'error'));
        }

        $user = (new User)->from_token($token);

        if (!$user) {
            $code = 401;
            $error = 'Invalid token';
            $this->json(compact('code', 'error'));
        }
        
        return $user;
    }

    protected function json(array $data = [])
    {
        header('Content-Type: application/json');

        echo json_encode($data);

        exit;
    }
}
