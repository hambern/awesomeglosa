<?php

class App
{
    protected $controller = 'home';

    protected $method = 'index';

    protected $params = [];

    public function __construct()
    {
        $url = $this->parse_url();

        if (isset($url[0]) && is_subclass_of($url[0], 'Controller')) {
            $this->controller = array_shift($url);
        }

        $this->controller = new $this->controller;

        if (isset($url[0]) && method_exists($this->controller, $url[0])) {
            $this->method = array_shift($url);
        }

        $this->params = $url;

        call_user_func_array([
            $this->controller,
            $this->method],
            $this->params);
    }

    protected function parse_url()
    {
        if (isset($_GET['url'])) {
            return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }

        return [];
    }
}
