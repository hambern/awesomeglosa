<?php

class Users extends Controller
{
    public function index()
    {
        $user = $this->auth();
        $code = 200;
        $error = null;

        $users = (new User)->all();
        
        $this->json(compact('users', 'code', 'error'));
    }

    public function create()
    {
        $token = null;
        $code = 200;
        $error = null;

        try {
            $token = (new User)->create($_POST);
        } catch (Exception $e) {
            $code = 401;
            $error = $e->getMessage();
        }

        $this->json(compact('token', 'code', 'error'));
    }

    public function login()
    {
        $token = null;
        $code = 200;
        $error = null;

        try {
            $token = (new User)->authenticate($_POST);
        } catch (Exception $e) {
            $code = 401;
            $error = $e->getMessage();
        }

        $this->json(compact('token'));
    }

    public function logout()
    {
        $user = $this->auth();
        $code = 200;
        $error = null;

        (new User)->update_token($user['id']);

        $message = 'Successfully logged out';

        $this->json(compact('message', 'code', 'error'));
    }
}