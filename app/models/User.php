<?php

class User extends Model
{
    public $table = 'users';

    private function generate_token($length = 64)
    {
        return substr(hash('sha256', random_bytes($length) . microtime()), 0, $length);
    }

    public function update_token($id)
    {
        $token = $this->generate_token();

        $this->query("UPDATE $this->table SET token = :token WHERE id = :id", [
            'token' => $token,
            'id' => $id,
        ]);

        return $token;
    }

    public function from_token($token)
    {
        $sql = "SELECT * FROM $this->table WHERE token = :token";

        $user = $this->query($sql, [
            'token' => $token
        ])->fetch();

        return $user ?: null;
    }


    public function authenticate($data)
    {
        if (empty($data['email']) || empty($data['password'])) {
            throw new Exception("Alla fält måste fyllas i");
        }

        $user = $this->query("SELECT * FROM $this->table WHERE email = :email", [
            'email' => $data['email']
        ])->fetch();

        if (!$user) {
            throw new Exception("Det finns ingen användare med den e-postadressen");
        }

        if (password_verify($data['password'], $user['password'])) {
            return $this->update_token($user['id']);
        } else {
            throw new Exception("Lösenordet stämde inte");
        }
    }

    public function create($data)
    {
        if (
            empty($data['email']) ||
            empty($data['password']) ||
            empty($data['password_confirm'])
        ) {
            throw new Exception("Alla fält måste fyllas i");
        }

        $sql = "SELECT * FROM $this->table WHERE email = :email";

        $existingUser = $this->query($sql, $data)->fetch();

        if ($existingUser) {
            throw new Exception("Det finns redan en användare med den e-postadressen");
        }

        if ($data['password'] !== $data['password_confirm']) {
            throw new Exception("Lösenorden stämde inte överens");
        }

        $password = password_hash($data['password'], PASSWORD_BCRYPT);
        
        $token = $this->generate_token();

        $sql = "INSERT INTO $this->table (email, password, token) VALUES (:email, :password, :token)";

        $this->query($sql, [
            'email' => $data['email'],
            'password' => $password,
            'token' => $token
        ]);

        return $token;
    }
}
