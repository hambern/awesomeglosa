<?php

spl_autoload_register(function ($class) {

    $files = glob('app/*/*.php', GLOB_NOSORT);

    foreach ($files as $file)
    {
        if (strpos($file, $class . '.php') !== false) {
            require_once $file;
            break;
        }
    }
});
