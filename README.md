# MVC

Detta är ett jätteenkelt MVC-ramverk som är skapat utifrån [Codecourses YouTube-serie](https://www.youtube.com/watch?v=OsCTzGASImQ&list=PLfdtiltiRHWGXVHXX09fxXDi-DqInchFD). Jag har dock gjort några små förändringar för att bättre passa vår utvecklingsmiljö. Studera koden noga och utgå ifrån den när du skapar ditt eget MVC-ramverk. Det är okej att göra förändringar här och där om du föredrar det.

## Applikationsflöde

Applikationen initieras från `index.php`, som startar en session och laddar `init.php`. `Init.php` konfigurerar vår miljö, inklusive autoladdning av klassfiler och instansiering av `App` klassen, som hanterar routing baserat på URL-anrop.

### Controllers

Controllers (`app/controllers`) hanterar interaktionen mellan användaren och applikationen, till exempel att visa sidor och bearbeta formulärdata.

### Models

Models (`app/models`) representerar och hanterar data, logik och regler i applikationen, ofta genom att interagera med en databas.

### Views

Views (`app/views`) innehåller presentationen av applikationen och genererar HTML-utdata baserat på data som mottagits från controllern.

### Core

Kärnfiler (`app/core`) inkluderar basen för MVC-strukturen, som `App`, `Controller`, och `Model` klasserna, som andra klasser ärver eller utnyttjar.

## Installation

1. Klona repo: `git clone https://gitlab.com/hambern/mvc.git`
2. Navigera till projektets rotkatalog.
3. Konfigurera `app/config.php` med din databasinformation.
4. Besök `index.php` via din webbläsare för att starta applikationen.

## Filstruktur

~~~
.
├── app
│   ├── config.php
│   ├── controllers
│   │   ├── home.php
│   │   ├── posts.php
│   │   └── users.php
│   ├── core
│   │   ├── App.php
│   │   ├── Controller.php
│   │   └── Model.php
│   ├── init.php
│   ├── models
│   │   ├── Post.php
│   │   └── User.php
│   └── views
│       ├── home
│       │   ├── contact.php
│       │   └── index.php
│       ├── posts
│       │   └── index.php
│       └── users
│           ├── create.php
│           ├── index.php
│           ├── login.php
│           └── show.php
├── assets
│   └── style.css
├── index.php
├── partials
│   ├── bottom.php
│   └── top.php
└── README.md
~~~
